Nous utilisons TraduXio, un environnement collaboratif pour la taduction de textes culturels qui permet la contribution collaborative sur notre glossaire. <br/><br/>Si vous souhaitez contribuer, suivez les étapes ci-dessous:
---------------

1. Suivez ce [lien](https://traduxio.org/works/e7f7e6527cb76828ee6cd8df810a973a?open=original) pour créer votre compte;
2. Vérifier votre compte via votre adresse mail (n'oubliez pas de regarder dans vos spams !);
3. Envoyez nous votre nom d'utilisateur à contact@internation.world pour être ajouté au *groupe de contributeurs Internation*
