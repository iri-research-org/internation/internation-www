Contexte
---------------

Le centenaire de la Société des Nations a eu lieu le 10 Janvier 2020, dans un contexte où l’humanité fait face à un état d’urgence absolu, ce qui est reflété, entre autres, par une montée  des politiques conservatrices et réactionnaires, la hausse des inégalités mondiales ainsi que de crises régionales alimentées aussi bien par des guerres économiques que par les conséquences tragiques du réchauffement climatique.

Sur ce point, le rapport publié en 2018 par le GIEC aggrave les estimations décrites dans son rapport datant de 2014, amenant le secrétaire général – Monsieur António Guterres – à alerter dans un discours prononcé le 10 septembre 2019 : « si nous ne changeons pas la situation en 2020, nous risquons de manquer l’occasion d’éviter un changement climatique hors de notre contrôle, avec des conséquences désastreuses pour les gens et tous les systèmes naturels qui nous soutiennent. » Prenant note du manque de volonté politique pour faire face à ce désastre, António Guterres a réitéré ses inquiétudes profondes cinq mois plus tard à Davos (Janvier 2019), affirmant que « nous sommes dans un monde dont les défis globaux sont de plus en plus intégrés, alors que les réponses sont de plus en plus fragmentées, ainsi, si cette tendance ne s’inverse pas, cela nous conduira au désastre. »

C’est dans ce contexte, en partageant le sentiment de désarroi exprimé par António Guterres, que nous soumettons l’ensemble de ces concepts théoriques et pratiques au secrétaire général de l’ONU comme à toutes les institutions qui souhaitent soigner le présent et l’avenir de l’humanité. 


Nos thèses
---------------

Le concept d’internation a été développé par Marcel Mauss dans le contexte de la création de la Société des Nations (1920). Mauss soutenait que la nation ne peut être dissoute dans l’international – c’est-à-dire que la localité ne peut être dissoute dans le global. En 2020, nous interprétons cette déclaration comme la nécessité de soigner les localités à de multiples échelles : les localités biologiques, comme les niches ; les localités sociales et techniques, comme les villes ou les nations ; la localité biosphérique de la terre, etc.

Le concept de localité a toujours été refoulé par l’économie et la politique, au bénéfice d’extrémistes qui le mobilisent à leurs propres fins. Il faut que la localité soit reconceptualisée en termes d’échelles différentes et envisagée d’un point de vue scientifique. L’argument scientifique pour le concept de localité est fondé sur plusieurs considérations : (i) nous considérons l’Anthropocène comme le résultat d’une augmentation de l’entropie sous toutes ses formes (thermodynamique, biologique et informationnelle) ; (ii) nous croyons que la macro-économie, fondée sur la mécanique Newtonienne, doit être réorganisée afin de réduire l’entropie, ce qui veut dire d’augmenter la néguentropie au sens de Schrödinger et l’anti-entropie au sens de Bailly, Longo et Montévil ; (iii) la néguentropie et l’anti-entropie peuvent, cependant, seulement être qualifiées d’un point de vue local ; (iv) cela veut dire que nous devons expérimenter un modèle économique et technologique susceptible de générer des localités soutenables, ouvertes, et néguentropiques afin de lutter contre l’Anthropocène.


Étape 1 – Londres, Serpentine Gallery, 22-23 Septembre 2018 – Work Marathon 2018
---------------
<br/>

Étape 2 – Paris, Maison Suger, Janvier-Juin 2019 – Remondialisation et Internation
---------------
<br/>

Étape 3 – Paris, Collège de France, 26-27 Février 2019
---------------
<br/>

Étape 4 – Paris, ENMI2019 Préparatoires, 2-3 Juillet 2019
---------------