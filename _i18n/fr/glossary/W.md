Web herméneutique et réseau social délibératif
---------------

•	Un web herméneutique est un web qui rend possible des pratiques d’interprétations actives et d’expressions singulières des individus, contrairement à l’internet des plateformes qui fonctionne sur la base de la captation des données et du calcul intensif qui leur est appliqué.
•	Un réseau social délibératif est un réseau social qui permet la constitution de groupes de pairs et la délibération rationnelle / le débat argumenté entre ces groupes, contrairement au modèle dominant qui relie des individus à des individus en fonction de leurs données et de leurs profils, les isolant ainsi dans des environnements informationnels fragmentaires et hyper-personnalisées (« bulles informationnelles »).

La constitution d’un web herméneutique et de réseaux sociaux délibératifs permettrait de mettre les plateformes numériques au service de la création de communautés capacitantes, et non plus de la captation et de l’exploitation des données par la data economy.

*Compléments : les fonctionnalités du web herméneutique et des réseaux sociaux délibératifs.*

La constitution d’un web herméneutique et de réseaux sociaux délibératifs suppose de repenser les architectures de réseaux et les formats de données, afin d’introduire de nouvelles fonctions contributives et interprétatives dans les formats du web actuel et les outils déjà existants.

Par exemple :

•	des fonctions d’annotation graphique et de catégorisation partagée permettant de confronter des prises de notes et des interprétations de contenus par des utilisateurs actifs ;

•	Des algorithmes d’analyse de données reposant sur une recommandation qualitative par l’analyse des annotations permettant la constitution de groupes d’interprétations ou d’affinités ;

•	de nouveaux types de réseaux sociaux fondés sur la mise en relation de groupes et non d’individus isolés (basés sur le concept d’individuation collective de Simondon), permettant la confrontation des interprétations, la controverse et la discussion argumentée, qui sont essentielles à l’exercice du débat public comme à la constitution des savoirs.