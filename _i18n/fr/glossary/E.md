Économie de la contribution
---------------

La thèse proposée par le modèle de l’économie contributive consiste à soutenir que l’augmentation de la productivité rendue possible par l’automatisation pourrait permettre de libérer les individus d’un certain nombre d’emplois prolétarisants, et ouvrir ainsi de nouveaux champs d’activité capacitantes et contributives. L’objectif d’une telle économie est de tirer profit du temps que l’automatisation permet de gagner dans la sphère productive, pour développer des processus de capacitation et de contribution permettant de produire et de partager les nouveaux savoirs nécessaires pour affronter les évolutions technologiques en cours (leurs effets psychiques, sociaux, politiques, écologiques).

La contribution suppose :

•   la participation choisie des individus à une activité capacitante ;

•   la création d’une valeur sociétale / pratique / thérapeutique à travers l’exercice de cette activité (qui doit être productrice d’anti-anthropie).


*Compléments : l’économie contributive tout contre l’économie de marché*

Là où l’économie de marché s’intéresse au producteur sous l’angle de la maximisation du profit, et au consommateur sous l’angle de la fonction d’utilité, et repose sur les valeurs d’usage et d’échange, l’économie de la contribution se caractérise par le fait que :

•   les acteurs économiques ne sont plus séparés en producteurs d’un côté et consommateurs de l’autre, ils sont « contributeurs » c’est-à-dire qu’ils partagent et produisent des savoirs utiles à la société et au territoire ;

•   la valeur produite par les contributeurs n’est pas intégralement monétarisable, elle constitue une externalité positive qui ne peut se réduire à la valeur d’échange ou d’usage car elle ne s’use pas avec le temps.


En effet, la valeur des savoirs n’augmente pas avec la rareté (contrairement à la valeur d’échange), et ne s’use pas avec le temps (contrairement à la valeur d’usage) : au contraire, elle se développe et s’enrichit avec le temps. Les effets thérapeutiques des savoirs se construisent progressivement et sur le long terme, et leur valeur augmente à mesure qu’ils sont partagés et pratiqués collectivement : les individus qui les échangent s’enrichissent ainsi mutuellement, en transformant et en diversifiant leurs façons de vivre et en améliorant la qualité de leur milieu et de leur quotidien – bref, en élargissant leurs possibilités d’existence et en s’encapacitant. Il faudra donc développer de nouvelles conceptions de la valeur et de l’utilité (valeur ou utilité « contributive », « sociétale », « pratique », « thérapeutique » ou « anti-entropique ») et surtout, de nouveaux indicateurs pour la mesurer.

L’économie de la contribution n’exclue pas les autres manières de produire et d’échanger, mais se conjugue avec elles, accepte les règles du jeu de l’échange monétaire, se préoccupe des choix d’investissement (particulièrement de ceux qui conduisent à la production de biens publics), et fait du don une modalité possible de la participation.

Elle doit tenir compte :

•   du modèle productif, qui doit composer avec la finitude des ressources naturelles et le caractère cumulatif des ressources liées à l’activité cognitive ;

•   du rapport entre la fonction de contribution et la refonte des solidarités, au-delà du solidarisme assurantiel de l’État providence ;

•   de la territorialisation de la fonction de contribution qui implique une redéfinition des effets d’agglomération et une réévaluation des politiques publiques ;

•   de l’exigence d’établir de nouvelles mesures et de nouveaux indicateurs de valeur.



Entropie
---------------

Tendance à la désorganisation, à la déstructuration et au désordre. Un processus entropique, en son sens élargi au-delà de la thermodynamique, est un processus au cours duquel un système tend à épuiser ses potentiels dynamiques et sa capacité de conservation ou de renouvellement.

*Compléments : l’entropie, une notion transversale*

•   Le concept d’entropie est apparu au XIXème siècle dans le champ de la physique thermodynamique et a été initialement forgé pour décrire la dissipation irréversible de l’énergie qui conduit au désordre généralisé de l’univers (l’univers est alors compris comme un processus d’expansion en déploiement, qui se désorganise de plus en plus jusqu’à sa disparition à l’infini).

•   Au XXème siècle, ces questions d’entropie ont été reprises par E. Schrodinger dans le champ de la biologie. Dans Qu’est-ce que la vie, il décrit l’activité des organismes vivants comme un processus « néguentropique », c’est-à-dire opposé au processus entropique global, qui ne permet pas de lui échapper (car tout organisme vivant finit par mourir) mais de lutter temporairement et localement contre lui. La néguentropie caractérise ainsi le vivant comme une lutte contre l’entropie.

•   La théorie de l’entropie a enfin été reprise dans le champ de la théorie de l’information (travaux de Shannon) et dans celui de la cybernétique (travaux de Wiener) : ces auteurs ont avancé la notion d’entropie informationnelle. Ils soutiennent que l’on peut définir l’information comme un rapport d’entropie et de néguentropie : la néguentropie désigne alors ce qui résiste contre le caractère insignifiant de l’information (le fait que l’information ne m’informe pas).
