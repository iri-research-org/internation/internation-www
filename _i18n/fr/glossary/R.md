Revenu contributif et emplois contributifs intermittents
---------------

Le modèle de l’économie contributive a pour fonction de répartir équitablement entre les citoyens le temps rendu disponible par l’automatisation de la production, et de mettre ce temps au service de la capacitation des habitants (rémunérée par un revenu contributif) et de leur contribution au développement anti-anthropique du territoire (dans le cadre d’emplois contributifs intermittents c’est-à-dire d’emplois intermittents au sein de projets labellisés contributifs).

Deux outils sont proposés à cette fin :

•	la mise en place d’un revenu contributif aura pour fonction de rémunérer les temps de capacitation des individus (au cours desquels ceux-ci partagent, pratiquent et produisent collectivement des savoirs) ;

•	les individus ne pourraient néanmoins se voir attribuer un tel revenu qu’à condition que les savoirs et capacités ainsi développés soient mis en œuvre de manière intermittentes dans le cadre d’emplois intermittents dans des projets labellisés contributifs pour le territoire, (les individus font ainsi profiter la société et le territoire des capacités qu’ils ont développées lors de leurs périodes de capacitation).

*Compléments : le modèle des intermittents du spectacle*

Le fonctionnement du revenu contributif s’inspire donc du régime des intermittents du spectacle : le financement des activités de capacitation préparatoires est conditionné par le retour du fruit de ces travaux vers la société sous forme d’emplois dans des projets labellisés. Le revenu contributif se distingue des revenus de subsistance (RSA ou revenu universel), même s’il peut leur être complémentaire : il constitue un droit, mais il est néanmoins conditionné à une participation à l’économie contributive et à son inscription dans le territoire. Cette participation suppose que les savoirs acquis pendant le temps de capacitation soient mis en œuvre dans le cadre d’emplois au service de projets labellisés comme contributifs ou anti-anthropiques par et pour le territoire – ce qui suppose le développement d’institutions de labellisation rendant possible la délibération et la décision collective autour de la valeur anti-anthropique / contributive d’une activité. En plus d’être un procédé simple de financement, l’emploi contributif intermittent permettra au contributeur de s’encapaciter, d’enrichir le territoire grâce aux nouveaux savoirs produits, et de transformer les emplois en activités de travail (toujours à la fois capacitantes et contributives).

