Prolétarianisation
---------------

La prolétarisation désigne le processus qui consiste à priver des individus (producteurs, consommateurs, concepteurs) de leurs savoirs (savoir-faire, savoir-vivre, savoir concevoir et théoriser). Un individu est prolétarisé quand il ne parvient pas à se réapproprier / à ré-intérioriser le savoir qui a été extériorisé (et souvent automatisé) dans un support technique. En effet, la transmission ou l’apprentissage d’un savoir suppose toujours que le savoir qui a été intériorisé psychiquement par certains individus (ceux qui transmettent) soit extériorisé techniquement (dans un support de mémoire), afin de pouvoir être ré-intériorisé psychiquement par d’autres individus (ceux qui apprennent), qui intériorisent le savoir en pratiquant collectivement les supports dans lesquels il est conservé. La « prolétarisation » se produit quand la réintériorisation du savoir extériorisé par les individus psychiques dans les supports techniques est rendue impossible, dans la mesure où les supports de mémoire ne sont pas socialisés ni pratiqués. Les individus se voient alors soumis aux savoirs extériorisés dans ces supports, au lieu d’utiliser ces supports pour transmettre et partager les savoirs.

*Compléments : les stades de la prolétarisation et la prolétarisation généralisée*

On distingue trois stades dans le processus de prolétarisation (qui correspondent à trois stades du capitalisme) :

1. la prolétarisation des savoir-faire au XIXème siècle, qui se produit à travers le développement du machinisme industriel et de la mise en œuvre de l’organisation scientifique du travail (capitalisme productiviste) ;

2. la prolétarisation des savoir-vivre au XXème siècle, qui se produit à travers le développement des industries culturelles de programme - médias de masse comme la radio, le cinéma, la télévision (capitalisme consumériste) ;

3. la prolétarisation des savoir-concevoir au XXIème siècle, qui se produit à travers le développement technologies numériques et algorithmiques - et de ce qu’on appelle « l’intelligence artificielle » (capitalisme computationnel).

•   Au XIXème siècle, le développement du machinisme industriel qui signe le début de l’Anthropocène engendre un premier processus de prolétarisation : les premiers individus touchés par la prolétarisation ont été les ouvriers, qui se sont vus privés de leurs savoir-faire extériorisés et automatisés dans des machines (supports de savoirs mécanique), auxquelles sont soumis les corps laborieux dans les fonctions de production.

•	Au cours du XXe siècle cependant, ce qui est prolétarisé n’est plus seulement le savoir-faire du producteur : c’est aussi le savoir-vivre du consommateur. En effet, un consommateur ne produit pas ses propres arts de vivre ou modes d’existence : ses manières de vivre lui sont imposées par le marketing et la publicité, à travers la captation de son attention par les médias de masse, fondés sur les technologies analogiques (cinéma, photographie, télévision). Les arts de vivres sont ainsi transformés en comportements de consommation.

•	Le XXIème siècle a mis en évidence que ce sont aussi désormais les concepteurs et les décideurs qui sont prolétarisés, à travers l’application du calcul intensif à des quantité massives de données (big data), qui court-circuite l’élaboration théorique, ou à travers les programmes informatiques fournissant des « systèmes automatiques d’aide à la décision », qui court-circuitent les processus délibératifs, interprétatifs et décisionnels. 

Ces trois stades ne se succèdent pas les uns aux autres, mais se superposent et se combinent, si bien qu’on assiste aujourd’hui à un phénomène de prolétarisation généralisée : tous les « sachants » sont dépossédés de leurs savoirs (extériorisation et désintégration combinée de tous les types de savoirs). C’est la raison pour laquelle une déprolétarisation (ou « encapacitation ») généralisée est nécessaire (et possible grâce à l’économie de contribution, qui valorise la pratique de savoirs).