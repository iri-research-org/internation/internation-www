Anthropocène
---------------

Ce terme a été proposé par le prix Nobel de chimie Paul Crutzen pour désigner l’ère géologique ayant débutée lorsque les activités humaines ont eu un impact global significatif sur l’écosystème terrestre et sur le devenir de la planète. Cette nouvelle ère aurait été amorcée à la fin du XVIIIème siècle avec la révolution industrielle et met en question la possibilité de la vie sur Terre. L’Anthropocène peut être décrit comme « Entropocène » dans la mesure où cette période correspond à une augmentation massive des taux d’entropie, aux niveaux physique (dissipation de l’énergie), biologique (destruction de la biodiversité) et psycho-social (destruction de la diversité culturelle et sociale).



Anti-entropie
---------------

L’anti-entropie est une tendance à la structuration, à la diversification, à la production de nouveauté. Elle s’explique par le fait que l’organisation des êtres vivants s’oppose localement et temporairement à la loi de l’augmentation inéluctable de l’entropie. L’anti-entropie est en cela le processus qui caractérise le vivant en tant qu’il lutte contre la dissipation de l’énergie et la désorganisation qui en résulte et en tant qu’il produit entre autres de nouvelles fonctions et de nouveaux organes. La notion a été généralisée pour décrire tout ce qui tend à créer de la différence, du choix ou du nouveau dans un système se développant dans le sens de sa propre conservation et/ou de sa transformation vers une amélioration.