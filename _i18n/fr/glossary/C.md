Capacités / Compétences

---------------

Le développement des capacités (ou « capacitation ») se distingue de l’acquisition de compétences.


•	Les compétences à acquérir précèdent l’individu qui est censé les acquérir : elles correspondent à des standards comportementaux prédéterminés auxquels l’individu doit se conformer (deux individus peuvent acquérir individuellement des compétences identiques, ils deviennent dès lors interchangeables). L’emploi repose sur la mise en œuvre de compétences préalablement acquises : les compétences sont acquises en vue de l’employabilité.


•	Les capacités correspondent au contraire aux possibilités d’existence singulières de chaque individu, que celui-ci ne peut exercer et actualiser qu’à partir du moment où il s’individue collectivement, c’est-à-dire, à partir du moment où il pratique et partage des savoirs avec d’autres individus, et s’encapacite ainsi (les capacités sont des expressions de la singularité des individus, mais elles supposent, pour se développer, la pratique collective d’un savoir). Les capacités se développent au cours des activités de travail.


*Compléments : automatismes (compétences) et désautomatisation (capacitation)*

S’il ne fait qu’appliquer des règles préétablies ou répéter des comportements acquis, l’individu n’exerce pas un savoir ni ne développe des capacités, mais met en œuvre des automatismes et des compétences. Ceux-ci sont évidemment nécessaires à la pratique du savoir, néanmoins, ils ne sont pas suffisants : pour qu’il y ait véritablement pratique de savoir ou développement des capacités (et non seulement application de compétences), il faut que l’individu puisse inventer, créer, produire de la nouveauté (et non pas répéter le même). Le savoir se définit avant tout par la possibilité de désautomatiser les automatismes acquis et d’inventer des capacités nouvelles, et non par le seul exercice de ces automatismes ou la seule mise en œuvre de compétences.