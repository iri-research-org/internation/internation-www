Context
---------------

The one-hundredth anniversary of the League of Nations will occur on the 10th of January 2020, in a context where the humanity face an absolute state of emergency reflected, among other things, by the rise of conservative and reactionary politicians, the dramatic growth of global inequalities and regional crisis fueled by economic wars, and the tragic consequences of global warming. On this point, the 2018 report released by the IPCC have worsened the estimation described in its 2014 report, leading the Secretary-General of the United Nations - Mr. António Guterres - to warn in a speech given on the 10th of September 2018 : *“If we do not change course by 2020, we risk missing the point where we can avoid runaway climate change, with disastrous consequences for people and all the natural systems that sustain us.”*. Observing the lack of political will to face this disaster, António Guterres reiterated his deep concerns five months later in Davos (January 2019), claiming that *“we are in a world in which global challenges are more and more integrated, and the responses are more and more fragmented, and if this is not reversed, it's a recipe for disaster”*. It is in this context, and sharing the feeling of disarray expressed by António Guterres, that we are submitting theoretical and practical set of concepts to the Secretary General of the UN and all the institutions to the attention of all the institutions caring for the present and future of human kind. 

Our Goal
---------------

Internation/Geneva2020 is an international group of researchers, academics, artists and citizens, founded on the initiative of the philosopher Bernard Stiegler (IRI, Ars Industrialis). On the occasion of the 100th anniversary of the League of Nations (Friday, 10 January 2020), we will launch a discussion in Geneva, hopefully at the level of the United Nations, about the necessity to find a way out from the Anthropocene. In order to do so, we would like to hold an event consisting in an official submission to the UN of the ensemble of the work produced by the Internation/Geneva2020 group, where we collectively articulate the scientific, legal, political, technological and economic issues raised by the Anthropocene. Indeed, we believe that there is no alternative but to genuinely address the theoretical and practical challenges of the Anthropocene, whose toxicity has resulted in an irrational economic organization incapable of taking care of the biosphere, of biodiversity and of human populations.

Thus, we propose the constitution of an international program closely articulating theoretical research and territorial experimentation for enabling the invention of truly sustainable economic, industrial and social models. These researches and experimentations should be cross-linked within the framework of what we call an *internation*.

Our Thesis
---------------

The concept of *internation* was developed by Marcel Mauss in the context of the creation of the League of Nations (1920).  Mauss distinguished his position by arguing that the nation cannot be dissolved into the international — that is, that the local cannot be dissolved in the global. In 2020, we interpret this statement as the necessity to take care of localities at multiple scales: biological localities such as niches; social and technical localities, such as cities or nations; the biospheric locality of the Earth, etc. 

The concept of locality has been constantly repressed by economics and politics, with extremists reaping the benefit out of this unthoughtfulness. *Locality* must be reconceptualized in terms of levels of localities, and considered from a scientific point of view. The scientific argument for the concept of locality is based on several considerations : (i) we consider the Anthropocene to be the problem of an increase of entropy in all its forms (thermodynamic, biological and informational) ; (ii) we believe that the macro-economy, which was based on Newtonian mechanics, must therefore be reorganized in order to reduce entropy, which means increasing negentropy in the sense of Schrödinger and anti-entropy in the sense of Bailly, Longo and Montévil ; (iii) Negentropy and anti-entropy can, however, only be qualified from a local point of view ; (iv) This means we have to experiment economic and technological model likely to create sustainable, open, and negentropic localities in order to fight against the Anthropocene. 

Step 1 – London, Serpentine Gallery, September 22nd-23rd, 2018 – Work Marathon 2018
---------------
<br/>

Step 2 – Paris, Maison Suger, January-June 2019 – Remondialisation et Internation
---------------
<br/>

Step 3 – Paris, Collège de France, February 26th-27th, 2019
---------------
<br/>

Step 4 – Paris, ENMI2019 Préparatoires, July 2nd-3rd, 2019
---------------
<br/>

Step 5 – Paris, Maison Suger, November-March 2019/2020 – Remondialisation, Localities and Modernity
---------------
<br/>

Step 6 – Paris, Centre Pompidou, ENMI2019, December 17th-18th, 2019
---------------
<br/>

Step 7 – Geneva, Club Suisse de la Presse, Press Conference, January 10th, 2019
---------------