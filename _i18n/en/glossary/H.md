Hermeneutic web and the deliberative social network
---------------

•   A hermeneutic web is one that enables individuals to practise active interpretations and singular expressions, unlike the internet of platforms that operates on the basis of capturing data and treating it through intensive computing.
•   A deliberative social network is one that enables the constitution of peer groups, and rational deliberation and debate between these groups,

unlike the dominant model that links individuals to other individuals according to their data and profiles, thus isolating them within fragmented and hyper-personalized informational environments (‘filter bubbles’).

The creation of a hermeneutic web and deliberative social networks would enable digital platforms to serve the creation of capacitating communities, rather than the capture and treatment of data by the data economy.

*Supplement: the functionalities of the hermeneutic web and deliberative social networks.*

The creation of a hermeneutic web and deliberative social networks entails rethinking network architectures and data formats, in order to introduce new contributory and interpretative functions into current web formats and already existing tools.

For example:

•   graphical annotation and shared categorization functions making it possible for active users to compare note-taking and content interpretation;

•   data analysis algorithms based on qualitative recommendation through the analysis of annotations enabling the constitution of groups of interpretations or affinities;

•   new types of social networks founded on linking groups rather than isolated individuals (and based on Simondon’s concept of collective individuation), enabling the conflict of interpretations, controversy and reasoned discussion, all of which are essential to the exercise of public debate and the constitution of knowledge.
