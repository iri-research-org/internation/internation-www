Entropy
---------------

The tendency towards disorganization, destructuration and disorder. Taken in its broadest sense, beyond thermodynamics, an entropic process is one that involves the tendency of a system to exhaust its dynamic potentials and its capacity for conservation or renewal.
Supplement: entropy, a transversal notion.

•   The concept of entropy appeared in the nineteenth century in the field of thermodynamic physics and was initially devised as a way of describing the irreversible dissipation of energy, leading to the generalized disorder of the universe (the universe being understood as an unfolding process of expansion, which becomes increasingly disorganized until its disappearance at infinity).

•   In the twentieth century, these questions of entropy were taken up by Erwin Schrödinger in the field of biology. In What is Life?, he describes the activity of living organisms as ‘negentropic’, that is, a process opposing the global entropic process, which enables us not to escape it (every living organism eventually dies) but to temporarily and locally struggle against it. Negentropy thus characterizes life as a struggle against entropy.

•   Finally, the theory of entropy has been taken up in the fields of information theory (in Shannon’s work) and cybernetics (in Wiener’s work). Putting forward the notion of informational entropy, these authors argue that information can be defined as a relationship between entropy and negentropy: in this case, negentropy refers to what resists the unsignifying character of information (the fact that information does not inform me).