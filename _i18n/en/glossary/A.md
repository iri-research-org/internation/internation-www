Anthropocene
---------------

This term was proposed by the Nobel Laureate and chemist Paul Crutzen to describe the geological era that commenced when human activity began to have a significant global impact on Earth’s ecosystem and the future of the planet. The opening of this new era occurred in the late eighteenth century with the industrial revolution and it now brings the very possibility of life continuing on Earth into question. The Anthropocene can be described as an ‘Entropocene’ insofar as this period corresponds to a massive increase in the rate of entropy at the physical level (dissipation of energy), the biological level (destruction of biodiversity) and the psycho-social level (destruction of cultural and social diversity).


Anti-entropy
---------------

Anti-entropy is a tendency towards structuration, diversification and the production of novelty. It is explained by the fact that the organization of living things locally and temporarily opposes the law of the inevitable increase in entropy. Anti-entropy is in this respect the process that characterizes life insofar as it struggles against the dissipation of energy and the disorganization that is its result, and insofar as it produces, among other things, new functions and new organs. The notion has been generalized to describe everything that tends to create difference, choice or novelty in a system, everything in the development of a system that tends to self-conservation and/or transformation towards improvement.