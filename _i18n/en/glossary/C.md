Capabilities / Skills
---------------

The development of capabilities (or ‘capacitation’) is distinct from the acquisition of competences or skills.

•   Skills to be acquired precede the individual who is supposed to acquire them: they correspond to predetermined behavioural standards to which the individual is required to conform (two individuals may individually acquire identical skills, in which case they become interchangeable). Employment is based on the use of previously acquired skills, and skills are acquired for the sake of employability.

•   Capabilities, on the contrary, correspond to each individual’s singular possibilities of existence, which individuals can exercise and actualize only starting from the moment when they individuate themselves collectively, that is, starting from the moment when an individual practices and shares knowledge with other individuals, and thus capabilizes himself or herself (capabilities are expressions of the singularity of individuals, but they presuppose, in order to develop, the collective practice of a knowledge). Capabilities are developed during work activities.

*Supplement: automatisms (skills) and dis-automatization (capacitation).*

If the individual only applies pre-established rules or repeats acquired behaviours, then no knowledge is exercised and no capabilities are developed, since the individual merely utilizes automatisms and skills. Automatisms and skills are obviously necessary for the practice of knowledge, but they are not sufficient: for there to be a genuine practice of knowledge or the development of capabilities (and not just the application of skills), the individual must be able to invent, create and produce novelty (and not just repeat the same). Knowledge is defined above all by the possibility of dis-automatizing acquired automatisms and the possibility of inventing new capabilities, rather than merely exercising automatisms or putting skills to use.


Contributory economy
---------------

The thesis proposed by the contributory economy model consists in arguing that the productivity increases made possible by automation could free individuals from a certain number of proletarianizing jobs, and thus open new fields of capacitating and contributory activity. The goal of such an economy is to take advantage of the gains in time enabled by automation in the productive sphere, in order to develop processes of capacitation and contribution that enable the producting and sharing of the new knowledge necessary to face ongoing technological evolution (and its psychic, social, political and ecological impacts).

Contribution requires:

•   individuals to choose to participate in a capacitating activity;

•   the creation of a social/practical/therapeutic value through the exercise of this activity (which must produce ant

*Supplement: contributory economy versus market *

Where the market economy is concerned with the producer in terms of the maximization of profit, and the consumer in terms of utility function, and is based on use and exchange values, the economy of contribution is characterized by the fact that:

•   economic actors are no longer separated into producers on one side and consumers on the other; they are ‘contributors’, that is, they share and produce knowledge that is useful to a society and a region;

•   the value produced by contributors is not entirely monetizable; it constitutes a positive externality that cannot be reduced to exchange or use value because it is not exhausted over time.

Indeed, (unlike exchange value) the value of knowledge does not increase with scarcity, and (unlike use value) is not exhausted over time: on the contrary, it develops and becomes richer with time. The therapeutic effects of knowledge build up progressively and over the long term, and its value increases to the extent that it is shared and practised collectively: individuals who exchange knowledge are mutually enriched, by transforming and diversifying their ways of life and by improving the quality of their milieu and their everyday lives – in short, by expanding the possibilities of their existence and by capabilizing themselves. Hence it will be necessary to develop new conceptions of value and utility (‘contributory’, ‘societal’, ‘practical’, ‘therapeutic’ or ‘anti-entropic’ value or utility), and, especially, new indicators by which to measure it.

The economy of contribution does not exclude other ways of producing and exchanging, but combines with them, accepts the rules of the game of monetary exchange, is concerned with investment choices (particularly those leading to the production of public goods) and makes giving a possible modality of participation.

It must take into account:

•   the productive model, which must deal with the finitude of natural resources and the cumulative character of resources related to cognitive activity;

•   the relationship between the contribution function and the recasting of solidarity, beyond the social insurance solidarity of the welfare state;

•   the territorialization of the contribution function, which involves a redefinition of conglomeration effects and a reassessment of public policies;

•   the requirement to establish new measures and indicators of value.


Contributory income and intermittent contributory employment
---------------

The role of the contributory economy model is to equitably distribute among citizens the time made available by the automation of production, and to put this time at the service of the capacitation of inhabitants (remunerated by contributory income) and their contribution to the anti-anthropic development of the region (within the framework of intermittent contributory employment, that is, intermittent employment in projects designated as contributory).

To that end, two tools are proposed:

•   setting up a contributory income scheme will serve to remunerate the capacitation time of individuals (during which they collectively share, practise and produce knowledge);

•   individuals can, however, receive such income only if the knowledge and capabilities developed in this way are intermittently used in the context of casual employment in projects designated as contributory for the region (individuals thereby allow society and the region to benefit from capabilities that are developed during their periods of capacitation).

*Supplement: the model of casual workers in the performing arts.*

The functioning of contributory income is inspired by the scheme set up in France for casual workers in entertainment and the performing arts [intermittents du spectacle]: the financing of preparatory capacitation activities is conditional upon the return of the fruits of this work to society in the form of employment in designated projects. Contributory income is distinct from universal basic income (UBI), although it can be complementary to it: it may indeed be a right, but it is nevertheless conditional upon participation in the contributory economy as it is inscribed in the region. This participation requires knowledge acquired during the capacitation period to be utilized in the context of employment in the service of projects designated as contributory or anti-anthropic by and for the region – which assumes the development of institutions for this designation, which will enable collective deliberation and decision-making about the anti-anthropic or contributory value of an activity. In addition to being a financing process, intermittent contributory employment enables the contributor to gain capabilities, enriching the region thanks to the new knowledge produced, and transforming jobs into work activities (always both capacitating and contributory.