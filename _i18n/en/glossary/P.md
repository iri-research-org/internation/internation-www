Proletarianization
---------------

Proletarianization refers to the process by which individuals (producers, consumers, conceptualizers) are deprived of their knowledge (knowledge of how to do/make, how to live, and how to conceptualize and theorize). An individual is proletarianized when he or she fails to reappropriate/re-interiorize knowledge that has been exteriorized (and often automated) in a technical support. Indeed, the transmission, learning or apprenticeship of or in a form of knowledge always presupposes that the knowledge which has been psychically interiorized by some individuals (those who transmit it) is technically exteriorized (in a medium, a memory-support), in order that it may be psychically re-interiorized by other individuals (those who learn), who internalize knowledge by collectively practising the supports or media within which it is conserved. ‘Proletarianization’ occurs when the re-interiorization of knowledge exteriorized by psychic individuals in technical supports is made impossible, to the extent that these memory supports are not socialized or practised. Individuals then submit to the exteriorized knowledge contained in these supports, instead of using these supports to transmit and share knowledge.

*Supplement: stages of proletarianization and generalized proletarianization.*

Three stages in the process of proletarianization can be distinguished (corresponding to three stages of capitalism):

1. the proletarianization of work-knowledge in the nineteenth century, which occurred through the development of industrial machinism and the implementation of the scientific organization of labour (productivist capitalism);

2. the proletarianization of life-knowledge in the twentieth century, which occurred through the development of the cultural program industries – mass media such as radio, cinema and television (consumerist capitalism);

3. the proletarianization of conceptual knowledge in the twenty-first century, which is occurring through the development of digital and algorithmic technologies – and of what is called ‘artificial intelligence’ (computational capitalism).

•   In the nineteenth century, the development of industrial machinism, which marked the beginning of the Anthropocene, generated a first process of proletarianization: the first individuals affected by proletarianization were workers, and what they were deprived of was their work-knowledge, exteriorized and automated in machines (supports of mechanical knowledge), to which labouring bodies submit in the functions of production.

•  During the twentieth century, however, what is proletarianized is no longer just the work-knowledge of the producer: it is also the life-knowledge of the consumer. Indeed, consumers are those who do not produce their own arts of living or modes of existence: their ways of living are imposed by marketing and advertising, through the capture of their attention by the mass media, based on analogue technologies (cinema, photography, television). Arts of living are thus transformed into consumer behaviour.

•   The twenty-first century has shown that it is now also designers, conceptualizers and decision-makers who are proletarianized, through the application of intensive computing to massive quantities of data (‘big data’), which bypasses theoretical elaboration, or else through computer programs providing ‘automatic decision support systems’, which short-circuit processes of deliberation, interpretation and decision-making.

Rather than unfolding one after the other, these three stages overlap and combine, so that what we are witnessing today amounts to generalized proletarianization: every kind of ‘knower’ finds themselves dispossessed of their knowledge (exteriorization and disintegration combining for all types of knowledge). This is why a generalized de-proletarianization (or ‘capabilization’) is necessary (and possible, thanks to the economy of contribution, which values the practice of knowledge).