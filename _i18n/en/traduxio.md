We are using TraduXio, an environment for participative cultural text translation which allows collaborative contribution on our glossary. <br/><br/>If you would like to contribute, follow the steps below:
---------------

1. Follow this [link](https://traduxio.org/works/e7f7e6527cb76828ee6cd8df810a973a?open=original) to create your account;
2. Verify your account on your email adress (don't forget to check your spams!);
3. Send us your username at contact@internation.world (to be added to the *Internation contributors’ group*) and the language(s) you would like to contribute with!

<h3><i>The actual version has been written by Anne Alombert, with the contributions of the Internation/Geneva2020 group. Translated by Daniel Ross. We warmly welcome new entries!</i></h3>