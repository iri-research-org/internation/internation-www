Paris, 11 November 2019 


Mr. António Guterres
Secretary-General 
United Nations
405 East 42nd Street, 
New York, NY, 
10017, USA


Dear Mr. Secretary-General,

As you have repeatedly pointed out, international efforts to commit to a greenhouse gas reduction strategy compatible with the objectives set by the Paris Agreement have largely been inadequate, despite the forecasts documented by the IPCC and various other groups, organizations, and teams of scientists. 

Often, the gap between what is needed and what is actually occurring is expressed in terms of a lack of will (political or collective) and the rise of apathy (political or collective). This situation, where we witness a collective incapacity to change course, worries everybody: investors, societies, and particularly the younger generations, who wonder what kind of world they will inherit. 

Given this state of emergency, the transdisciplinary collective Internation/Geneva 2020 was formed at the Serpentine Galleries in London on 22 September 2018, on the initiative of Hans Ulrich Obrist and Bernard Stiegler. It comprises fifty-two members from around the world, including scientists, mathematicians, legal scholars, economists, philosophers, anthropologists, sociologists, doctors, artists, engineers, business leaders, activists, and designers.

We argue that the overall lack of will is a symptom of a profound disorientation concerning the challenges at stake in our contemporary era, the Anthropocene. In the absence of a theoretical framework allowing us to have an adequate understanding of such challenges, any actions that hold the potential to avoid runaway climate change are hindered. Our main thesis is that the Anthropocene can be described as an Entropocene, insofar as the contemporary period is above all characterized by a process of the massive increase of entropy in all its forms (physical, biological, informational). However, the question of entropy has been neglected by a mainstream economics. We therefore think a new economic model designed for combating the production of entropy is needed.

In order to scientifically investigate these problems and to invent democratic solutions, we believe new research methods are required that we call *contributory researches*. They seek to bring together researchers from various academic fields and territorial actors<span title="Such as civil societies, economic actors, politicians, institutions, engaged in a transdisciplinary approach and participatory processes."><sup>1</sup></span> into new networks of research and experimentation, in an approach similar to that you have called “*inclusive multilateralism*”. In this way, territories would be able to experiment sustainable, solvent and desirable economic activities and technological tools. The aim would be to lead local societies to develop reproducible recommendations, through rapid transfer processes.

Adopting such a territorialized approach could be the occasion to reread the reflections by the anthropologist Marcel Mauss published under the title *La nation*. In 1920, Mauss recommended that the development of internationalism should not be on the cost of territorial and cultural specificity. From this perspective, he outlined the concept of *internation*, a dynamic according to which nations could be called upon to cooperate without erasing their local dimensions.

A century after the establishment of the League of Nations, it is with reference to this work that we believe such an internation should be set up as the institutional framework of a new inclusive multilateralism. Its function would be to encourage, launch, support, and evaluate local experimentations. This process could be initiated with a call for tenders inviting actors from candidate territories to collectively engage, via networks, in contributory research approaches. 

In order to establish a set of specifications for such territorial laboratory initiatives, the Internation/Geneva 2020 Collective has defined a set of theoretical questions and thematic axes that would structure this approach, briefly described in the Appendix.

Some of this work, will be discussed in general terms next December at the Centre Pompidou (Paris), for which representatives of *Youth for Climate*, the movement popularized by Greta Thunberg, will be invited.

We would like to present this work to yourself and staff, and to make it public in Geneva, if possible during a press conference gathering different stakeholders (such as UN staff, political & business leaders, civil movements and academics). Given the dramatic importance of these issues and in the hope of launching an international debate, we would be grateful to hold this event on the historic grounds of Palais des Nations around the centenary of the foundation of the League of Nations, which will be celebrated on 10 January 2020.

In thanking you for your action and for the attention brought to this initiative, we ask that you believe, Mr. Secretary-General, in our very respectful devotion.

For the Internation/Geneva 2020 Collective.

Hans Ulrich Obrist, Director of the Serpentine Galleries 
(London) 

Bernard Stiegler, President of the Institut de recherche 
et d’innovation (Paris)
