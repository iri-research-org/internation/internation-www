# About

This website presents the writing process of the Memorandum of Understanding which will be presented to the United Nations in Geneva, in January 2020.

The project is carried by a group of academics, artists and citizens involved seriously into the understanding and trying to give an analysis of the Anthropocene's sickness.

If you want to contribute to the project, you can contact the maintainers of this repository
and read the CONTRIBUTING.md file


# Credits

internation.world based on Hyperspace by HTML5 UP | html5up.net | @ajlkn
Jekyll version by Andrew Banchich | gitlab.com/andrewbanchich
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)

Credits:

		jQuery (jquery.com)
		html5shiv.js (@afarkas @jdalton @jon_neal @rem)
		Misc. Sass functions (@HugoGiraudel)
		Respond.js (j.mp/respondjs)
		Skel (skel.io)

Repository [Jekyll logo](https://github.com/jekyll/brand) icon licensed under a [Creative Commons Attribution 4.0 International License](http://choosealicense.com/licenses/cc-by-4.0/).
