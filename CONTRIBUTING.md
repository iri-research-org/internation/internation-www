# Local installation for contribution

* install [Ruby](https://www.ruby-lang.org/en/downloads/) and [Rubygems](https://rubygems.org/pages/download)

* clone this repository

* To install the environment, run :

```
gem install bundler
bundle config set path 'vendor/bundle'
bundle install
```

* To test the server, run :

```
bundle exec jekyll serve
```