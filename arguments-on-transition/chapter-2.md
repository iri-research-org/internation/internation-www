---
layout: memorandum
permalink: /arguments-on-transition/chapter-2/

title: '2. Localities, territories and urbanities in the age of platforms and confronted to the challenges of the Anthropocene era'
authors: 'Giacomo Gilmozzi, Olivier Landau, Bernard Stiegler, David Berry, Sara Baranzoni, Pierre Clergue and Anne Alombert'
chapter: arguments-on-transition/chapter-2.md
id: 'two'

navigation:
 - title: '2. Localities, territories and urbanities in the age of platforms and confronted to the challenges of the Anthropocene era'
   id: 'two'
   link: 'chapter-2'

video: 
  - link: 'https://media.iri.centrepompidou.fr/video/ldtplatform/serpentinegalleries2018/map_room_panel05_2018-09-22_web.mp4#t=0:20:23,1:06:51'
    title: 'David Berry, <i>Really Smart Cities, Platforms and Infrasomatization</i> -- Sara Baranzoni, <i>Urban Metabolism</i>, Serpentine Galleries (September 2018)'
  - link: 'https://media.iri.centrepompidou.fr/video/ldtplatform/serpentinegalleries2018/serpentine_2018-09-22.mp4#t=0:03:28,2:03:28'
    title: 'Giacomo Gilmozzi, <i>Smart cities or Smart (Data) Mines?,</i> London, Serpentine Galleries’ Work Marathon, September 2018.'
  - link: 'https://media.iri.centrepompidou.fr/video/internation/sara_baranzoni-day_1_guayaquil_archipelago_conference.mp4'
    title: 'Sara Baranzoni, <i>Archipelago: The Rythme of the City</i>, Guayaquil, Ecuador, July 2019.'    
---
