---
layout: memorandum
permalink: /arguments-on-transition/chapter-9/

title: '9. Planetary Detox and the Neurobiology of Ecological Collapse'
authors: 'Gerald Moore, Nikolaos A. Mylonas, Marie-Claude Bossière, Anne Alombert and Marco Pavanini'
chapter: arguments-on-transition/chapter-9.md
id: 'nine'

navigation:
 - title: '9. Planetary Detox and the Neurobiology of Ecological Collapse'
   id: 'nine'
   link: 'chapter-9'
---
