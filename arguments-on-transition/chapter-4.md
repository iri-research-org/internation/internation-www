---
layout: memorandum
permalink: /arguments-on-transition/chapter-4/

title: '4. Contributory research and social (self) sculpture'
authors: 'Noel Fitzpatrick, Anne Alombert, Colette Tron, Glenn Loughran, Yves Citton and Bernard Stiegler'
chapter: arguments-on-transition/chapter-4.md
id: 'four'

navigation:
 - title: '4. Contributory research and social (self) sculpture'
   id: 'four'
   link: 'chapter-4'

video: 
- link: 'https://media.iri.centrepompidou.fr/video/ldtplatform/serpentinegalleries2018/map_room_panel06_2018-09-22_web.mp4#t=0:01:12,0:58:52'
  title: 'Anne Alombert, Noel Fitzpatrick, Glenn Loughran and Vincent Puig, <i>Contributive Research, Social Sculpture, Art & Technology</i>, Serpentine Galleries (September 2018)'
---
