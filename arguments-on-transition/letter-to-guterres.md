---
layout: memorandum
permalink: /arguments-on-transition/letter-to-guterres/
redirect_from:
  - /arguments-on-transition/

# LETTER TO GUTERRES

title: 'LETTER TO ANTÓNIO GUTERRES'
authors: 'Bernard Stiegler and Hans Ulrich Obrist'
chapter: arguments-on-transition/letter-to-guterres.md
id: 'letter'
main-title: true

navigation:

 - title: 'LETTER TO ANTÓNIO GUTERRES'
   id: 'letter'
   link: 'letter-to-guterres'
---
