---
layout: memorandum
permalink: /arguments-on-transition/general-introduction/

# INTRODUCTION

title: 'GENERAL INTRODUCTION: DECARBONIZATION AND DEPROLETARIANIZATION<br><br><i>GAGNER SA VIE AU XXIè SIÈCLE</i>'
authors: 'Bernard Stiegler, Paolo Vignola and Mitra Azar<br>Translated by Daniel Ross'
chapter: arguments-on-transition/general-introduction.md
id: 'intro'
main-title: true

navigation:

 - title: 'GENERAL INTRODUCTION: DECARBONIZATION AND DEPROLETARIANIZATION<br><br><i>GAGNER SA VIE AU XXIè SIÈCLE</i>'
   id: 'intro'
   link: 'general-introduction'
---