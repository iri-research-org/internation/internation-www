---
layout: memorandum
permalink: /arguments-on-transition/chapter-8/

title: '8. Ethos and Technologies'
authors: 'Michał Krzykawski and Susanna Lindberg
revised by David Bates'
chapter: arguments-on-transition/chapter-8.md
id: 'eight'

navigation:
 - title: '8. Ethos and Technologies'
   id: 'eight'
   link: 'chapter-8'
---
