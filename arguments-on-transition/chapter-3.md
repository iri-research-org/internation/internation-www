---
layout: memorandum
permalink: /arguments-on-transition/chapter-3/

title: "3. Contributory economy: capacitatory's territorial processes and new accountability"
authors: 'Clément Morlat, Olivier Landau, Théo Sentis, Franck Cormerais, Anne Alombert and Michał Krzykawski'
chapter: arguments-on-transition/chapter-3.md
id: 'three'

navigation:
 - title: "3. Contributory economy: capacitatory's territorial processes and new accountability"
   link: 'chapter-3'
---
