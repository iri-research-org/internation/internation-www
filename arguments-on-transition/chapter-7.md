---
layout: memorandum
permalink: /arguments-on-transition/chapter-7/

title: '7. Contributory design and deliberative technologies: towards a social generativity in the automatic societies'
authors: 'Anne Alombert, Vincent Puig and Bernard Stiegler'
chapter: arguments-on-transition/chapter-7.md
id: 'seven'

navigation:
 - title: '7. Contributory design and deliberative technologies: towards a social generativity in the automatic societies'
   id: 'seven'
   link: 'chapter-7'
---
