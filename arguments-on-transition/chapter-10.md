---
layout: memorandum
permalink: /arguments-on-transition/chapter-10/

title: '10. Carbon and silicon: contribution to a critique of political economy'
authors: 'Daniel Ross'
chapter: arguments-on-transition/chapter-10.md
id: 'ten'

navigation:
 - title: '10. Carbon and silicon: contribution to a critique of political economy'
   id: 'ten'
   link: 'chapter-10'
---