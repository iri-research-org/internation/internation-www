---
layout: memorandum
permalink: /arguments-on-transition/chapter-1/

title: '1. Anthropocene, exosomatization and negentropy'
authors: 'Maël Montévil, Bernard Stiegler, Giuseppe Longo, Ana Soto and Carlos Sonnenschein'
chapter: arguments-on-transition/chapter-1.md
id: 'one'

navigation:
 - title: '1. Anthropocene, exosomatization and negentropy'
   id: 'one'
   link: 'chapter-1'

video: 
- link: 'https://media.iri.centrepompidou.fr/video/ldtplatform/serpentinegalleries2018/serpentine_2018-09-22.mp4#t=0:03:28,2:03:28'
  title: 'Bernard Stiegler, <i>The Final Warning</i>, Serpentine Galleries (September 2018)'
---
