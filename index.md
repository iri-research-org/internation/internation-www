---
layout: home
title: INTERNATION

# Sidebar navigation menu and sections contents
navigation:

 - title: home.link_1.title
   home_title: home.link_1.home_title
   text: home.link_1.description
   id: 'one'
   url: about
   button: home.link_1.button

 - title: home.link_2.title
   home_title: home.link_2.home_title
   text: home.link_2.description
   id: 'two'
   url: arguments-on-transition
   class: 'ltag'
   button: home.link_2.button

 - title: home.link_3.title
   home_title: home.link_3.home_title
   text: home.link_3.description
   id: 'three'

 - title: home.link_4.title
   home_title: home.link_4.home_title
   text: home.link_4.description
   id: 'four'
---