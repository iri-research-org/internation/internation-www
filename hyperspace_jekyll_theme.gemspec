# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "internation_website"
  spec.version       = "0.1.1"
  spec.authors       = ["Riwad Salim", "Yves-Marie Haussonne"]
  spec.email         = ["dev@internation.world"]

  spec.summary       = %q{The internation.world website based on jekyll and hyperspace-jekyll theme}
  spec.homepage      = "https://internation.world"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_development_dependency "jekyll", "~> 3.8"
  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 12.3"
end
