---
layout: press
title: pages.press.title
description: pages.press.description
redirect_from:
  - /live/
  
permalink: /press/
---
<br>

<!-- <div class="articles">
<div class="french-press"> -->
<br>

## French/Swiss articles
<br>

[Le blog de Médiapart de l'Association des Amis de la Génération Thunberg](https://blogs.mediapart.fr/les-amis-de-la-generation-thunber)
<br>

[Le SauteRhin, Bernard Umbrecht, Sept. 27, 2020 –– A propos du livre Bifurquer : 1. Qu’appelle-t-on bifurquer ?](https://www.lesauterhin.eu/a-propos-du-livre-bifurquer-1-quappelle-t-on-bifurquer/)
<br>

[Le Vent Se Lève, Anne Alombert, Sept. 1, 2020 –– Bifurquer : Bernard Stiegler et le Collectif Internation pour des "territoires existentiels"](https://lvsl.fr/bifurquer-bernard-stiegler-et-le-collectif-internation-pour-des-territoires-existentiels/)
<br>

[Libération, Nicolas Celknik, Mar. 9, 2020 –– Bernard Stiegler : «Même s’ils le voulaient, les Etats n’auraient pas les concepts pour changer»](https://www.liberation.fr/debats/2020/03/08/bernard-stiegler-meme-s-ils-le-voulaient-les-etats-n-auraient-pas-les-concepts-pour-changer_1780988)
<br>

[La Croix, Marine Lamoureux, Jan. 10, 2020 –– Face à la crise climatique et sociale, jusqu’où revoir nos modèles ?](https://www.la-croix.com/Debats/Forum-et-debats/Face-crise-climatique-sociale-jusquou-revoir-modeles-2020-01-10-1201070866)
<br>

[Le Temps, Stéphane Bussard, Mar. 7, 2020 –– Un collectif scientifique vient au secours de Greta Thunberg](https://www.letemps.ch/monde/un-collectif-scientifique-vient-secours-greta-thunberg)
<br>

[Up Magazine, Bernard Umbrecht, Jan 13, 2020 –– Internation : la pensée à la rescousse de l’ONU pour sauver le monde](https://up-magazine.info/index.php/decryptages/analyses/29088-29088/)
<br>

[Europe1, Nathalie Levy, Jan. 22, 2020 –– Donald Trump qui attaque Greta Thunberg à Davos ? "C'est du Trump tout craché"](https://www.europe1.fr/international/davos-lavenir-de-la-planete-passera-t-il-par-les-entreprises-3944646)
<br>

[RT France (Interdit d'interdire), Frédéric Taddeï, Jan. 3, 2020 –– (vidéo) Bernard Stiegler : face à l'urgence climatique, que faire ?](https://francais.rt.com/magazines/interdit-d-interdire/70280-bernard-stiegler-face-urgence-climatique-que-faire)

<!-- </div> -->
<!-- <div class="italian-press"> -->
<br>

## Italian articles
<br>

[L'Espresso, Marco Pacini, Feb. 12, 2020 –– Stiegler: «Studiosi di tutto il mondo uniamoci per Greta, in nome della terra»](https://espresso.repubblica.it/plus/articoli/2020/02/12/news/adulti-per-greta-thunberg-1.343939?preview=true)
<br>

[Il Venerdì di Repubblica, Stefano Simoncini, Feb. 28, 2020 –– Evadiamo dalla galera digitale](https://internation.world/documents/2venerdirepubblica_stiegler.pdf)
<br>

[Operaviva, Peppe Allegri, Feb. 10, 2020 –– Dentro, oltre e contro la società automatica](https://operavivamagazine.org/dentro-oltre-e-contro-la-societa-automatica/)
<br>


## Podcasts
<br>

[RFI (C'est pas du vent), Anne-Cecile Bras, Jan. 22, 2020 –– Bernard Stiegler: panser le monde en suivant Greta Thunberg](http://www.rfi.fr/fr/podcasts/20200123-stiegler-philosophe-panser-monde-greta-thunberg-davos-forum-climat)
<br>

[RT France (Interdit d'interdire), Frédéric Taddeï, Jan. 3, 2020 –– Bernard Stiegler : face à l'urgence climatique, que faire ?](https://soundcloud.com/user-504397555/bernard-stiegler-face-a-lurgence-climatique-que-faire)
<br>

<!-- </div>
</div> -->

<!-- French articles | Italian articles
------------ | -------------
Libération, Nicolas Celknik, Mar. 9, 2020 | [L'Espresso (Italy), Marco Pacini, Feb. 12, 2020](https://espresso.repubblica.it/plus/articoli/2020/02/12/news/adulti-per-greta-thunberg-1.343939?preview=true) 
[La Croix, Marine Lamoureux, Jan. 10, 2020](https://www.la-croix.com/Debats/Forum-et-debats/Face-crise-climatique-sociale-jusquou-revoir-modeles-2020-01-10-1201070866) | [Il Venerdì di Repubblica (Italy), Stefano Simoncini, Feb. 28, 2020]()
[Le Temps, Stéphane Bussard, Mar. 7, 2020](https://www.letemps.ch/monde/un-collectif-scientifique-vient-secours-greta-thunberg) | [Operaviva, Peppe Allegri, Feb. 10, 2020](https://operavivamagazine.org/dentro-oltre-e-contro-la-societa-automatica/)
[Up Magazine, Bernard Umbrecht, Jan 13, 2020](https://up-magazine.info/index.php/decryptages/analyses/29088-29088/) | 
[RFI (c'est pas du vent), Anne-Cecile Bras, Jan. 22, 2020](http://www.rfi.fr/fr/podcasts/20200123-stiegler-philosophe-panser-monde-greta-thunberg-davos-forum-climat) | 
[Europe1, Nathalie Levy, Jan. 22, 2020](https://www.europe1.fr/international/davos-lavenir-de-la-planete-passera-t-il-par-les-entreprises-3944646) | 

[RT France, Frédéric Taddeï, Jan. 3, 2020](https://francais.rt.com/magazines/interdit-d-interdire/70280-bernard-stiegler-face-urgence-climatique-que-faire) | 
[Médiapart blog, Friends of Thunberg's generation](https://blogs.mediapart.fr/les-amis-de-la-generation-thunber) |  -->