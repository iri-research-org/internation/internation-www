---
layout: glossary 
title: pages.glossary.title
permalink: /glossary/

contribution: traduxio.md

glossary:
 - letter: 'A'
   id: 'one'
   words: glossary/A.md

 - letter: B
   id: 'two'
   words: glossary/B.md

 - letter: C
   id: 'three'
   words: glossary/C.md
     
 - letter: D
   id: 'four'
   words: glossary/D.md

 - letter: E
   id: 'five'
   words: glossary/E.md

 - letter: F
   id: 'six'
   words: glossary/F.md

 - letter: G
   id: 'seven'
   words: glossary/G.md

 - letter: H
   id: 'eight'
   words: glossary/H.md

 - letter: I
   id: 'nine'
   words: glossary/I.md

 - letter: J
   id: 'ten'
   words: glossary/J.md

 - letter: K
   id: 'eleven'
   words: glossary/K.md

 - letter: L
   id: 'twelve'
   words: glossary/L.md

 - letter: M
   id: 'thirteen'
   words: glossary/M.md

 - letter: 'N'
   id: 'fourteen'
   words: glossary/N.md

 - letter: O
   id: 'fifteen'
   words: glossary/O.md

 - letter: P
   id: 'sixteen'
   words: glossary/P.md

 - letter: Q
   id: 'seventeen'
   words: glossary/Q.md

 - letter: R
   id: 'eighteen'
   words: glossary/R.md

 - letter: S
   id: 'nineteen'
   words: glossary/S.md

 - letter: T
   id: 'twenty'
   words: glossary/T.md

 - letter: U
   id: 'twenty-one'
   words: glossary/U.md

 - letter: V
   id: 'twenty-two'
   words: glossary/V.md

 - letter: W
   id: 'twenty-three'
   words: glossary/W.md

 - letter: X
   id: 'twenty-four'
   words: glossary/X.md

 - letter: 'Y'
   id: 'twenty-five'
   words: glossary/Y.md

 - letter: Z
   id: 'twenty-six'
   words: glossary/Z.md
---
